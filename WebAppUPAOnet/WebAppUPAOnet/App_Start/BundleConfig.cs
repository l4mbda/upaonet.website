﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace WebAppUPAOnet.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
   
        
            //Hoja de estilo de Apariencia
            bundles.Add(new StyleBundle("~/Content/Apariencia").Include(
                "~/Content/font-awesome.min.css",
                "~/Content/slicknav.css",
                "~/Content/style.css",
                "~/Content/responsive.css",
                "~/Content/animate.css"
                ));

            //Hoja de Estilo de color de la Apariencia
            bundles.Add(new StyleBundle("~/Content/Color").Include(
                "~/Content/colors/color-emblem.css"
                ));

            // Hoja de estilo personalizado
            bundles.Add(new StyleBundle("~/Content/StyleLogin").Include(
                "~/Content/styleLogin.css"
                ));

            //Scripts Animacion
            bundles.Add(new ScriptBundle("~/Scripts/Apariencia").Include(
                "~/Scripts/jquery-2.1.4.min.js",
                "~/Scripts/jquery.migrate.js",
                "~/Scripts/modernizrr.js",
                "~/Scripts/js/bootstrap.min.js",
                "~/Scripts/jquery.fitvids.js",
                "~/Scripts/owl.carousel.min.js",
                "~/Scripts/nivo-lightbox.min.js",
                "~/Scripts/jquery.isotope.min.js",
                "~/Scripts/jquery.appear.js",
                "~/Scripts/count-to.js",
                "~/Scripts/jquery.textillate.js",
                "~/Scripts/jquery.lettering.js",
                "~/Scripts/jquery.easypiechart.min.js",
                "~/Scripts/jquery.nicescroll.min.js",
                "~/Scripts/jquery.parallax.js",
                "~/Scripts/mediaelement-and-player.js",
                "~/Scripts/jquery.slicknav.js",
                "~/Scripts/script.js"
                ));
        }
    }
}